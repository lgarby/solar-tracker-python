'''
@file 

@brief 

@details

@author Logan Garby

@date Jan 25th, 2021
'''

import pyb
from adcDriver import adcDriver
import ihm04a1
import utime

class solarTracker:
    
    # NOTES:
    #   x axis is along the longer length of the solar panel meaning that the 
    #   motor which rotates around this axis is the primary actuator (M1),
    #   connected to the base pole, and the LDR's ratios which determine is 
    #   rotation is necessary are LDR1/LDR2 and LDR3/LDR4
    #
    #   y axis is along shorter panel length, with panel rotation controlled
    #   by the secondary actuator (M2). The revelant LDR ratios for the y-axis
    #   are between LDR1/LDR4 and LDR2/LDR3
    
    
    def __init__(self,ratio):
        '''
        @param ratio The ratio of LDR resistances to initiate panel motion
        '''
        self.LDR1 = adcDriver('A2',1000)
        self.LDR2 = adcDriver('A3',1000)
        self.LDR3 = adcDriver('D12',1000)
        self.LDR4 = adcDriver('D11',1000)
        
        ## Input parameter - minimum ratio of LDR resistances that causes motion
        self.ratio = ratio
        
        ## Motor 1 is connected to the base pole and tilts the primary axis
        self.M1 = ihm04a1.MoDrive (pyb.Pin.cpu.A10, pyb.Pin.cpu.B4, 
                                    pyb.Pin.cpu.B5,pyb.Timer (3), 1, 2, 1000)
        
        ## Motor 2 is responsible for the actuator on the secondary axis
        self.M2 = ihm04a1.MoDrive (pyb.Pin.cpu.C1, pyb.Pin.cpu.A0, 
                                    pyb.Pin.cpu.A1, pyb.Timer (5), 1, 2, 1000)
       
        self.R1 = 0
        self.R2 = 0
        self.R3 = 0
        self.R4 = 0
        self.Rtup = (0, 0, 0, 0)
        self.minRindex = 0
        self.R_min = 0
        self.R_xdir = 0
        self.R_ydir = 0
        
    def run(self):
        
        self.LDR1.run()
        self.LDR2.run()
        self.LDR3.run()
        self.LDR4.run()
        
        utime.sleep_ms(100)
        
        self.R1 = self.LDR1.resistance
        self.R2 = self.LDR2.resistance
        self.R3 = self.LDR3.resistance
        self.R4 = self.LDR4.resistance
       
        self.Rtup = (self.R1, self.R2, self.R3, self.R4)
        
        # print(str(self.Rtup))
        
        # Finding index and value of LDR in the brightest light (least resistance)
        self.R_min = min(self.Rtup)
        self.minRindex = self.Rtup.index(self.R_min)
        
        
        
        # If LDR1 is the minimum resistance (most sunlight)
        if self.minRindex == 0:
            self.R_xdir = self.R2
            self.R_ydir = self.R4
        
        # If LDR2 is minimum
        elif self.minRindex == 1:
            self.R_xdir = self.R1
            self.R_ydir = self.R3
        
        # If LDR3 is minimum
        elif self.minRindex == 2:
            self.R_xdir = self.R4
            self.R_ydir = self.R2
        
        # If LDR4 is minimum
        else: # self.minRindex == 3:
            self.R_xdir = self.R3
            self.R_ydir = self.R1
        
        
        self.R_x_ratio = self.R_xdir/self.R_min
        self.R_y_ratio = self.R_ydir/self.R_min
        
        #   These if statements check if there is a large difference is LDR 
        #   resistances in the y-axis (x-axis rotation) and sets the duty cycle
        #   based on which photoresistor is brighter.
        if self.R_x_ratio > self.ratio:
           
            if self.minRindex == 0 or self.minRindex == 3:    
                self.M1.set_duty(100)
            else: # minRindex == 1 or 2
                self.M1.set_duty(-100)
                
        else:
            self.M1.off()
            
        #   These if statements check if there is a large difference is LDR 
        #   resistances in the x-axis (y-axis rotation) and sets the duty cycle
        #   based on which photoresistor is brighter.    
        if self.R_y_ratio > self.ratio:
           
            if self.minRindex == 0 or self.minRindex == 1:    
                self.M2.set_duty(100)
            else: # minRindex == 2 or 3
                self.M2.set_duty(-100)
                
        else:
            self.M2.off()

            
    def stop(self):
        
        self.M1.off()
        self.M2.off()
            
if __name__ == '__main__':

    task = solarTracker(3)
    
    while True:
        
        try:
            task.run()
        except TypeError:
            print('Type Error')
            task.stop()
            break
        except KeyboardInterrupt:
            task.stop()
            print(task.R1)
            print(task.R2)
            print(task.R3)
            print(task.R4)
            break
            