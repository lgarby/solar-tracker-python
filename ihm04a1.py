# -*- coding: utf-8 -*-
#
## @file ihm04a1.py
#      This file contains a driver for the IHM04A1 accessory board for the 
#      STM32 Nucleo. The IHM04A1 board contains one L6206PD dual DC motor 
#      driver chip.
#
#  @author: JR Ridgely (revised for CP Solar Regatta by Logan Garby)


import pyb


# ============================================================================

## This class contains a driver for one half of the L6206PD motor driver 
#  chip on an STM32 Nucleo accessory board. The driver should work for L6206 
#  chips on other boards with a few pin changes as needed. Each L6206 has two 
#  H-bridges, and to use two motors one can make two objects of this class, 
#  one for each half of the driver chip. 
#
#  Note L6206 switching frequency should not exceed 100KHz (datasheet).

class MoDrive:

    # ------------------------------------------------------------------------
    ## Initialize the driver for the L6206 motor driver chip by putting the
    #  pins used in the appropriate modes and ensuring that power to the motor
    #  is turned off. For the two sides of the L6206 on an IHM04A1 board, the
    #  constructor calls should be:
    #  @code
    #  m1 = ihm04a1.MoDrive (pyb.Pin.cpu.A10, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5,
    #           pyb.Timer (3), 1, 2, 1000)
    #  m1 = ihm04a1.MoDrive (pyb.Pin.cpu.C1, pyb.Pin.cpu.A0, pyb.Pin.cpu.A1,
    #           pyb.Timer (5), 1, 2, 1000)
    #  @endcode
    #  @param EN_pin The pin used for this H-bridge's EN/OCD line
    #  @param IN1_pin The pin used for the IN1 line
    #  @param IN2_pin The pin used for the IN2 line
    #  @param timer The timer used to make PWM signals for IN1 and IN2
    #  @param ch1 The timer channel used for IN1's PWM
    #  @param ch2 The timer channel used for IN2's PWM
    #  @param freq The timer's PWM frequency, default 1000 Hz

    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer, ch1, ch2, freq=1000):

        self._EN_pin = EN_pin
        self._timer = timer

        # Initialize the enable pin as open-drain with a pullup
        self._EN_pin.init (mode = pyb.Pin.OUT_OD, pull = pyb.Pin.PULL_UP)

        # Initialize the timer to be used for the PWM
        self._timer.init (freq=freq)
        self._ch1 = self._timer.channel (ch1, pyb.Timer.PWM, pin=IN1_pin)
        self._ch2 = self._timer.channel (ch2, pyb.Timer.PWM, pin=IN2_pin)

        # Set the enable pin off and the motor PWM outputs to be at 0% duty
        self._EN_pin.low ()
        self._ch1.pulse_width_percent (0)
        self._ch2.pulse_width_percent (0)    
        

    # ------------------------------------------------------------------------
    ## Set the duty cycle for the motor. If a positive duty cycle is given, 
    #  the motor is pushed in one direction; if a negative duty cycle is 
    #  given, the motor is pushed in the opposite direction.

    def set_duty (self, percent):

        # Make sure the percentage isn't greater than 100%
        percent = percent if percent <=  100 else  100
        percent = percent if percent >= -100 else -100

        # If percentage is negative, go "backwards" with positive duty cycle
        if percent < 0:
            self._ch1.pulse_width_percent (0)
            self._ch2.pulse_width_percent (-percent)
        else:
            self._ch1.pulse_width_percent (percent)
            self._ch2.pulse_width_percent (0)

        # Set the enable bit so the motor can be powered
        self._EN_pin.high ()


    # ------------------------------------------------------------------------
    ## Set the enable pin low so that the half-bridges are in high impedance
    #  mode and the motor can freewheel. 

    def freewheel (self):

        self._EN_pin.low ()


    # ------------------------------------------------------------------------
    ## Set the frequency of the PWM used by this motor's timer to the given
    #  value in Hertz. 
    #  @param freq New value for the PWM frequency

    def set_freq (self, freq):

        self._timer.init (freq=freq)



    def off(self):
        # Set the enable pin off and the motor PWM outputs to be at 0% duty
        self._EN_pin.low ()
        self._ch1.pulse_width_percent (0)
        self._ch2.pulse_width_percent (0)



