'''
@file testLDR.py

@brief Used the adcDriver to test the four LDR array

@details Runs the adcDriver and returns each photoresistor's voltage and
    resistance

@author Logan Garby

@date Jan 25th, 2021
'''

from adcDriver import adcDriver

class testLDR:
    
    def __init__(self):
        
        self.LDR1 = adcDriver('A2',1000)
        self.LDR2 = adcDriver('A3',1000)
        self.LDR3 = adcDriver('D12',1000)
        self.LDR4 = adcDriver('D11',1000)
        
    def run(self):
        
        self.LDR1.run()
        self.LDR2.run()
        self.LDR3.run()
        self.LDR4.run()
        
        # print('V1 = {:}, V2 = {:}'.format(self.LDR1.voltage, self.LDR2.voltage))
        # print('R1 = {:}, R2 = {:}'.format(self.LDR1.resistance, self.LDR2.resistance))
        print(self.LDR1.voltage)
        print(self.LDR2.voltage)
        print(self.LDR3.voltage)
        print(self.LDR4.voltage)
        
        print(self.LDR1.resistance)
        print(self.LDR2.resistance)
        print(self.LDR3.resistance)
        print(self.LDR4.resistance)


if __name__ == '__main__':    
    
    task = testLDR()
    task.run()        