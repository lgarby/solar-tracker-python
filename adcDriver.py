'''
@file adcDriver.py

@brief This file sets up the ADC for a given pin and voltage divider.

@details The file conains the adcDriver class that sets up and ADC pin object
    for the Cal Poly Solar Regatta 2020-2021 senior project LDR array. 

@author Logan Garby

@date Jan 25th, 2021
'''


import pyb

class adcDriver:
    '''
    @breif This class initalized and ADC object, reads and returns the voltage.
    
    @details This class is designed to be used by the solar panel tracking 
        mechanism LDR array to calculate the resistance of the LDR using the
        voltage across a voltage divider.
    '''
    
    def __init__(self, pin, R1):
        ''' Initializes all objects needed for reading the LDR resistance
        and voltage.
        
        @param pin ADC input pin [string] (e.g. 'A0')
        @param R1 Resistance in Ohms of the resistor for the associated pins 
            voltage divider [int]
        '''
        self.Pin = pyb.Pin(pin)         # Creates Pin object
        self.ADC = pyb.ADC(self.Pin)    # Create ADC for prev pin
        
        self.R1 = R1
        self.read_val = 0
        self.voltage = 0
        self.resistance = 0
        
    def run(self):
        ''' Reads the ADC voltage, calculates the resistance, and creates 
        class objects for the voltage and resistance. 
        '''
        self.read_val = self.ADC.read()
        
        self.voltage = 3.3*(self.read_val/4095)
        
        if self.voltage != 0 and self.voltage != 3.3:
            self.resistance = self.R1/(3.3/self.voltage - 1)
        else:
            self.resistance = 'Undefined'