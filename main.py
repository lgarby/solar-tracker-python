'''
@file main.py

@brief Main file for CP Solar Regatta solar panel tracking system

@details This file runs two solarTracker tasks simultaneously to achieve
    two axis solar panel tracking. This project was made for the Cal Poly
    Solar Regatta 2020-2021 senior project group's solar panel tracking
    mechanism project

@author Logan Garby

@date March 18th, 2021
'''

from solarTracker import solarTracker

xTask = solarTracker(1,2,10)
yTask = solarTracker(2,2,10)

while True:
    
    try:
        xTask.run()
        yTask.run()

    except KeyboardInterrupt:
        xTask.STOP()
        yTask.STOP()
        print(xTask.state)
        print(yTask.state)
        print(xTask.R1)
        print(xTask.R2)
        print(xTask.R3)
        print(xTask.R4)
        break