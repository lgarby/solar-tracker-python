var classsolarTrackerOld_1_1solarTracker =
[
    [ "__init__", "classsolarTrackerOld_1_1solarTracker.html#a7ff523d8c8179cb04784df4437ab0c1d", null ],
    [ "run", "classsolarTrackerOld_1_1solarTracker.html#acf2ad8ceb79ab683c0c93ddb31647ab7", null ],
    [ "stop", "classsolarTrackerOld_1_1solarTracker.html#ab1c5892f8bf6e5f3b7f61082eb1f7392", null ],
    [ "LDR1", "classsolarTrackerOld_1_1solarTracker.html#a71d231099e0708fb2cf3e1e0b65030e9", null ],
    [ "LDR2", "classsolarTrackerOld_1_1solarTracker.html#a7dc620fd80cbf5e8359891d61498f4eb", null ],
    [ "LDR3", "classsolarTrackerOld_1_1solarTracker.html#a2e50cdd8225c521890e68e105f9936b3", null ],
    [ "LDR4", "classsolarTrackerOld_1_1solarTracker.html#a0213d0459d4264cb6bc3055e11e287ec", null ],
    [ "M1", "classsolarTrackerOld_1_1solarTracker.html#a4302ded5007f8013d7a16db95c6be589", null ],
    [ "M2", "classsolarTrackerOld_1_1solarTracker.html#a969ab4fc004e1d934632e410802d3fdd", null ],
    [ "minRindex", "classsolarTrackerOld_1_1solarTracker.html#aafd61b5f8a7d87c6709013f73c09e2d6", null ],
    [ "R1", "classsolarTrackerOld_1_1solarTracker.html#ad6101d16801582f3be7fbe3b776b992c", null ],
    [ "R2", "classsolarTrackerOld_1_1solarTracker.html#adb3effe70329f7610f47d9eaa4a1a3c1", null ],
    [ "R3", "classsolarTrackerOld_1_1solarTracker.html#a373ac40486bde105363d063a71ca4674", null ],
    [ "R4", "classsolarTrackerOld_1_1solarTracker.html#a9878a205764a49cd47a44b32a31064a1", null ],
    [ "R_min", "classsolarTrackerOld_1_1solarTracker.html#aabcea2b07def2bf1e8a2c301c22373a0", null ],
    [ "R_x_ratio", "classsolarTrackerOld_1_1solarTracker.html#a315da490efd83b3912bf1d7de5207376", null ],
    [ "R_xdir", "classsolarTrackerOld_1_1solarTracker.html#a80bef864368d99060023a8bbc3832fd7", null ],
    [ "R_y_ratio", "classsolarTrackerOld_1_1solarTracker.html#aaf80b487c4adc3941028b2d98295ecac", null ],
    [ "R_ydir", "classsolarTrackerOld_1_1solarTracker.html#a79c39250ff062f0c7cdbddea952694bd", null ],
    [ "ratio", "classsolarTrackerOld_1_1solarTracker.html#a96d0e7829713143b3cf738e2015580df", null ],
    [ "Rtup", "classsolarTrackerOld_1_1solarTracker.html#ab4524e53058f6ee58dfb685fdb560743", null ]
];