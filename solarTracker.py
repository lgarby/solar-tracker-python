'''
@file solarTracker.py

@brief Contains solarTracker class with FSM for 1 axis of solar panel tracking

@details This is the main class used by main.py for the solar panel tracking
    mechanism. 

@author Logan Garby

@date March 18th, 2021
'''

import pyb
from adcDriver import adcDriver
import ihm04a1
import utime

class solarTracker:
    ''' 
    @brief Class that reads LDR data and actuates the motors accordingly
    
    @details This class contains a finite state machine and multiple methods
        which get the resistance information of the photosensors from the 
        adcDriver class and perform logic on the resistances in order to 
        determine how to power the linear actuators (connected to the IHM04A1 
        motor driver). adcDriver and ihm04a1 are both needed for this class.
    '''
    # NOTES:
    #   x axis is along the longer length of the solar panel meaning that the 
    #   motor which rotates around this axis is the primary actuator (M1),
    #   connected to the base pole, and the LDR's ratios which determine is 
    #   rotation is necessary are LDR1/LDR2 and LDR3/LDR4
    #
    #   y axis is along shorter panel length, with panel rotation controlled
    #   by the secondary actuator (M2). The revelant LDR ratios for the y-axis
    #   are between LDR1/LDR4 and LDR2/LDR3
    
    S0_INIT = 0
    S1_STOPPED = 1
    S2_FWD_MOTION = 2
    S3_REV_MOTION = 3
    
    def __init__(self,motor_number,ratio,interval):
        '''
        @param motor_number Motor number designation (1 for primary x axis/2 
        for secondary y axis) [int]
        @param ratio The ratio of LDR resistances to initiate panel motion. 
            Set to 2 for direct sunlight. [int]
        @param interval Interval at which the finite state machine (FSM) is 
            run. [int]
        '''
        self.LDR1 = adcDriver('A2',1000)
        self.LDR2 = adcDriver('A3',1000)
        self.LDR3 = adcDriver('D12',1000)
        self.LDR4 = adcDriver('D11',1000)
        
        ## Input parameter - minimum ratio of LDR resistances that causes motion
        self.setRatio = ratio
        
        if motor_number == 1:
            ## Motor 1 is connected to the base pole and tilts the primary axis
            self.Motor = ihm04a1.MoDrive (pyb.Pin.cpu.A10, pyb.Pin.cpu.B4, 
                                        pyb.Pin.cpu.B5,pyb.Timer (3), 1, 2, 1000)
        elif motor_number == 2:    
            ## Motor 2 is responsible for the actuator on the secondary axis
            self.Motor = ihm04a1.MoDrive (pyb.Pin.cpu.C1, pyb.Pin.cpu.A0, 
                                        pyb.Pin.cpu.A1, pyb.Timer (5), 1, 2, 1000)
        else:
            pass
        
        # Initializes all motor pins low
        self.Motor.off()
        
        ## Motor number input (1 for x axis motor, 2 for y)
        self.motor_number = motor_number   
        
        ## LDR 1 resistance class attribute
        self.R1 = 0
        ## LDR 2 resistance class attribute
        self.R2 = 0
        ## LDR 3 resistance class attribute
        self.R3 = 0
        ## LDR 4 resistance class attribute
        self.R4 = 0
    
        ## One of 2 LDR ratios for a specified axis
        self.Ratio_1 = 0
        ## Second LDR ratio for a specified axis
        self.Ratio_2 = 0
        
        ## FSM run interval in ms
        self.interval = interval    
        ## utime ms attribute for the current run time
        self.time = utime.ticks_ms()
        ## Next timestamp object
        self.next_time = utime.ticks_add(self.time, self.interval)  

        ## Attribute for current FSM state
        self.state = self.S0_INIT

    def run(self):
        ''' Runs one iteration of the solar panel tracking task finite state 
        machine
        '''

        self.time = utime.ticks_ms()
        
        self.checkLDR()
        self.determineNewState()
        
        if utime.ticks_diff(self.time, self.next_time) >= 0:
            
            if (self.state == self.S0_INIT):
                pass
                
            if (self.state == self.S1_STOPPED):
                
                self.STOP()
                
            elif(self.state == self.S2_FWD_MOTION):
                
                self.FWD()
                
            elif(self.state == self.S3_REV_MOTION):
                
                self.REV()
                
            else:
                pass
            
            # Next timestamp
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            
            
    def determineNewState(self):
        ''' Uses the values from checkLDR() to determine which state the FSM 
        should reside in next.
        '''
        if self.Ratio_1 >= self.setRatio or self.Ratio_2 >= self.setRatio:
                    self.transitionTo(self.S2_FWD_MOTION)
        
        elif 1/self.Ratio_1 >= self.setRatio or 1/self.Ratio_2 >= self.setRatio:
            self.transitionTo(self.S3_REV_MOTION)
            
        else:
            self.transitionTo(self.S1_STOPPED)
                
        
    def checkLDR(self):
        ''' Reads all 4 LDR's and creates or updates class objects with their
        values. These if statements check if there is a large difference is LDR 
        resistances in the x-axis (y-axis rotation) and sets the duty cycle
        based on which photoresistor is brighter.   
        '''
        self.LDR1.run()
        self.LDR2.run()
        self.LDR3.run()
        self.LDR4.run()
        
        self.R1 = self.LDR1.resistance
        self.R2 = self.LDR2.resistance
        self.R3 = self.LDR3.resistance
        self.R4 = self.LDR4.resistance
        
        if self.motor_number == 1:
            self.Ratio_1 = self.R2/self.R1
            self.Ratio_2 = self.R3/self.R4
        elif self.motor_number == 2:
            self.Ratio_1 = self.R4/self.R1
            self.Ratio_2 = self.R3/self.R2            
        else:
            pass
        
            
    def FWD(self):
        '''
        Actuates the selected motor in the positive direction
        '''
        self.Motor.set_duty(100)
        
    def REV(self):
        '''
        Actuates the selected motor in the negative direction
        '''
        self.Motor.set_duty(-100)
    
    def STOP(self):
        '''
        Stops motor
        '''
        self.Motor.off()

        
    def transitionTo(self, next_state):
        '''
        This method transitions to the next state
        @param next_state Designation for next state
        '''
        self.state = next_state
 


if __name__ == '__main__':

    task = solarTracker(1,3,10)
    
    while True:
        
        try:
            task.run()

        except KeyboardInterrupt:
            task.STOP()
            print(task.state)
            print(task.R1)
            print(task.R2)
            print(task.R3)
            print(task.R4)
            break
            